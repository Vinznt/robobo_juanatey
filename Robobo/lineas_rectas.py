from Robobo import Robobo
from robobo_video.robobo_video import RoboboVideo
import numpy as np
import cv2 as cv
from time import time


def do_canny(gray):
    # Applies a 5x5 gaussian blur with deviation of 0 to frame
    blur = cv.GaussianBlur(gray, (5, 5), 0)
    # Applies Canny edge detector with minVal of 150 and maxVal of 170
    canny = cv.Canny(blur, 150, 170)
    return canny


def do_segment(img):
    # Since an image is a multi-directional array containing the relative intensities of
    # each pixel in the image, we can use frame.shape to return a tuple:
    # [number of rows, number of columns, number of channels] of the dimensions of the frame
    # Since height begins from 0 at the top, the y-coordinate of the bottom of the frame is its height
    # Creates a rectangular polygon for the mask defined by four (x, y) coordinates
    polygons = np.array([[(20, 512), (492, 512), (410, 390), (95, 390)]])
    # Creates an image filled with zero intensities with the same dimensions as the frame
    mask = np.zeros_like(img)
    # Allows the mask to be filled with values of 1 and the other areas to be filled with values of 0
    cv.fillPoly(mask, polygons, 255)
    # A bitwise_and operation between the mask and frame keeps only the rectangular area of the frame
    segment = cv.bitwise_and(img, mask)
    return segment


def calculate_lines(img, lines):
    # Empty arrays to store the coordinates of the left and right lines
    left = []
    right = []
    horizontal = []
    # Loops through every detected line
    if lines is not None:
        for line in lines:
            # Reshapes line from 2D array to 1D array
            x1, y1, x2, y2 = line.reshape(4)
            # Fits a linear polynomial to the x and y coordinates and
            # returns a vector of coefficients which describe the slope and y-intercept
            parameters = np.polyfit((x1, x2), (y1, y2), 1)
            slope = parameters[0]
            y_intercept = parameters[1]
            # If slope is negative, the line is to the left of the lane,
            # otherwise, the line is to the right of the lane
            if abs(y1 - y2) < 4:
                horizontal.append((slope, y_intercept))
            else:
                if slope < 0:
                    left.append((slope, y_intercept))
                else:
                    right.append((slope, y_intercept))
    # Averages out all the values for left and right into a single slope and y-intercept value for each line
    left_avg = np.average(left, axis=0)
    right_avg = np.average(right, axis=0)
    # horizontal_avg = np.average(horizontal, axis=0)
    # Calculates the x1, y1, x2, y2 coordinates for the left and right lines
    left_line = calculate_coordinates(img, left_avg)
    right_line = calculate_coordinates(img, right_avg)
    # horizontal_line = calculate_coordinates(img, horizontal_avg)
    return np.array([left_line, right_line])


def calculate_coordinates(img, parameters):
    try:
        slope, intercept = parameters
        # Sets initial y-coordinate as height from top down (bottom of the frame)
        y1 = img.shape[0]
        # Sets final y-coordinate as 100 above the bottom of the frame
        y2 = int(y1 - 100)
        # Sets initial x-coordinate as (y1 - b) / m since y1 = mx1 + b
        x1 = int((y1 - intercept) / slope)
        # Sets final x-coordinate as (y2 - b) / m since y2 = mx2 + b
        x2 = int((y2 - intercept) / slope)
    except:
        x1 = y1 = x2 = y2 = 0
    return np.array([x1, y1, x2, y2])


def visualize_lines(img, lines):
    # Creates an image filled with zero intensities with the same dimensions as the frame
    lines_visualize = np.zeros_like(img)
    # Checks if any lines are detected
    try:
        for x1, y1, x2, y2 in lines:
            # Draws lines between two coordinates with green color and 5 thickness
            cv.line(lines_visualize, (x1, y1), (x2, y2), (0, 255, 0), 5)
    except:
        pass
    return lines_visualize


def do_lines(img):
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    canny = do_canny(gray)
    cv.imshow('canny', canny)
    segment = do_segment(canny)
    hough = cv.HoughLinesP(segment, 1, np.pi/180, 30, np.array([]), minLineLength=30, maxLineGap=20)
    # Averages multiple detected lines from hough into one line for left border of lane and one line for right border
    param = calculate_lines(img, hough)
    # Visualizes the lines
    lines_visualize = visualize_lines(img, param)
    # Overlays lines on frame by taking their weighted sums and
    # adding an arbitrary scalar value of 1 as the gamma argument
    img = cv.addWeighted(img, 0.9, lines_visualize, 1, 1)
    return img, param


def round_unit(num):
    """
    Round any decimal number to the nearest unit
    :param num: Float number
    :return: Int number rounded to unity
    """
    try:
        neg = False
        if num < 0:
            num = abs(num)
            neg = True
        if (num - int(num)) > 0.5:
            num = int(num) + 1
        else:
            num = int(num)
        if neg:
            num = -num
    except ValueError:
        pass
    return num


def steering(param, i):
    global vel, lista_ep, lista_incr, kp
    if (param[0][0] == 0) and (param[1][0] == 0):
        rob.stopMotors()
    else:
        if param[0][0] == 0:
            param[0][2] = param[1][2] - 269
        if param[1][0] == 0:
            param[1][2] = param[0][2] + 269
        centre = (param[0][2]+param[1][2])/2
        ep = (256-centre)  # position error
        print(ep)
        lista_ep.append(ep)
        incr = abs(ep) * kp  # control solo proporcional
        lista_incr.append(incr)  # Guardo los valores de incremento y error en el tiempo

        # Cada vez que se han guardado más de 50 elementos en la lista
        # elimino el valor de la posición 0 para no sufrir desbordamiento
        if i > 50:
            lista_ep.pop(0)
            lista_incr.pop(0)

        # Limitador del valor de salida
        if incr < 0:
            incr = 0
        # se permite que el máximo incremento de velocidad  para una velocidad dada sea siempre menor que 15
        elif incr > 30:
            incr = 30

        # redondea a la unidad más cercana el valor del incremento ya que el método moveWheels solo acepta enteros
        incr = round_unit(incr)

        # CONTROL DE VELOCIDAD
        if ep > 0:
            rob.moveWheels(vel, vel - incr)
        else:
            rob.moveWheels(vel - incr, vel)


if __name__ == '__main__':
    rob = Robobo('127.0.0.1')  # 127.0.0.1
    rob.connect()  # 10.113.36.167
    vid = RoboboVideo("127.0.0.1")
    vid.connect()
    rob.moveTiltTo(105, 100)
    i = 1
    vel = 30
    lista_ep = []
    lista_incr = []
    kp = 0.065
    while True:
        start_time = time()  # start time of the loop
        frame = vid.getImage()
        frame, coord = do_lines(frame)
        cv.imshow('Smartphone camera', frame)
        # Controls the robot
        # steering(coord, i)
        i += 1
        # program ends when press 'q'
        if cv.waitKey(1) & 0xFF == ord('q'):
            cv.destroyAllWindows()
            break
        # print("FPS: ", 1.0 / (time() - start_time))  # FPS = 1 / time to process loop
