from Robobo import Robobo
from time import time
import keyboard
from utils.StatusFrequency import StatusFrequency
from utils.Color import Color
from utils.LED import LED
from utils.Wheels import Wheels
from utils.Emotions import Emotions
from utils.IR import IR


def robobo():
    rob.moveTiltTo(90, 100)
    while True:
        if keyboard.read_key() == 'flecha arriba':
            rob.moveWheels(20, 20)
        elif keyboard.read_key() == 'flecha abajo':
            rob.moveWheels(-20, -20)
        elif keyboard.read_key() == 'flecha izquierda':
            rob.moveWheels(20, 0)
        elif keyboard.read_key() == 'flecha derecha':
            rob.moveWheels(0, 20)
        else:
            rob.moveWheels(0, 0)


if __name__ == '__main__':
    rob = Robobo('127.0.0.1')  # 127.0.0.1
    rob.connect()  # 10.113.36.167
    rob.changeStatusFrequency(StatusFrequency.Max)
    start_time = time()
    robobo()
    elapsed_time = time() - start_time
    print("Elapsed time: %.3f seconds." % elapsed_time)
