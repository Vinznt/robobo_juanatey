from Robobo import Robobo
from time import time
from utils.StatusFrequency import StatusFrequency
from utils.Color import Color
from utils.LED import LED
from utils.Wheels import Wheels
from utils.Emotions import Emotions
from utils.IR import IR


def robobo():
    for _ in range(20):
        rob.moveWheels(20, 20)
        while rob.readIRSensor(IR.FrontC) < 120 and \
                rob.readIRSensor(IR.FrontRR) < 120 and rob.readIRSensor(IR.FrontLL) < 120:
            rob.wait(0.1)
        rob.moveWheelsByTime(-20, -20, 1)
        rob.moveWheelsByTime(-20, 20, 1)


if __name__ == '__main__':
    rob = Robobo('127.0.0.1')  # 127.0.0.1
    rob.connect()  # 10.113.36.167
    rob.changeStatusFrequency(StatusFrequency.Max)
    start_time = time()
    robobo()
    elapsed_time = time() - start_time
    print("Elapsed time: %.3f seconds." % elapsed_time)
