import sim
import numpy as np
import cv2


class RoboboVideo:

    def __init__(self, ip):
        sim.simxFinish(-1)  # just in case, close all opened connections
        self.clientID = sim.simxStart('127.0.0.1', 19997, True, True, 5000, 5)
        if self.clientID != -1:
            print('Connected to remote API server')
        else:
            print('Failed connecting to remote API server')

    def connect(self):
        res, self.cameraHandler = sim.simxGetObjectHandle(self.clientID, 'Smartphone_camera',
                                                          sim.simx_opmode_oneshot_wait)
        sim.simxGetVisionSensorImage(self.clientID, self.cameraHandler, 0, sim.simx_opmode_streaming)

    def getImage(self):
        err, resolution, image = sim.simxGetVisionSensorImage(self.clientID, self.cameraHandler, 0,
                                                              sim.simx_opmode_buffer)
        if err == sim.simx_return_ok:
            img = np.array(image, dtype=np.uint8)
            img.resize([resolution[1], resolution[0], 3])
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            return cv2.flip(img, 0)
        elif err == sim.simx_return_novalue_flag:
            print("no image yet")
            pass
        else:
            print(err)

    def disconnect(self):
        pass
